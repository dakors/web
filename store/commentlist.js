export const state = () => ({
    result: [],
    page: 1,
})

export const mutations = {
    SET_RESULT(state , info) {
        state[info.blog_id].result = info.result;
    },
    ADD_RESULT(state , info) {
        //console.log(state);
        //console.log(info);
        state[info.blog_id].result.push(...info.result);
    },
    UPDATE_RESULT(state , info) {
        //console.log(info['index']);
        //console.log(info.item);
        state[info.blog_id].result.splice( info['index'], 1, info.item);
        //console.log(state[info.blog_id].result[info['index']]);
    },
    ADD_TOPRESULT(state , info) {
        //state.result.unshift(...info);
    },
    DEL_RESULT(state , info) {
        state[info.blog_id].result.splice( info['index'] , 1);
    },
    SET_PAGE(state , info) {
        state[info.blog_id].listpage = info.listpage;
    },
    INIT(state , info) {
        state[info.blog_id] = { result: [], listpage : 1}
    }
};

export const actions = {
}