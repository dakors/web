export const state = () => ({
    'featurephoto': {
        data : {
            background:'',
            photo:'',
            nick_name:'',
            Fans:0,
        }
    },
    recommended:{},
    options:{
        gender:[ '未標明' , '男性' , '女性' ],
        location : [ '不限地區', '台北市', '新北市', '桃園市', '台中市',
            '台南市', '高雄市', '基隆市', '新竹市', '嘉義市', '新竹縣',
            '苗栗縣', '彰化縣', '南投縣', '雲林縣', '嘉義縣',
            '屏東縣', '宜蘭縣', '花蓮縣', '台東縣', '澎湖縣',
            '金門縣', '連江縣', '馬祖縣']
    },
    friendlist:[],
    requests:'',
})

export const mutations = {
    SET_FEATURE_PHOTO(state , info) {
        state.featurephoto = info;
    },
    SET_RECOMMANDED(state , info) {
        state.recommended = info;
    },
    SET_REQUESTS(state , info) {
        state.requests = info;
    },
    ADD_FRIENDLIST(state , info) {
        state.friendlist.push(...info);
    },
    CLEAN_FRIENDLIST(state , info) {
        state.friendlist = [];
    },
    login (state, information) {
        return this.$auth.loginWith('local', {
            data: information
        }).catch((err)=> {
            var msg = this.$errormsg(err.response.data);
            this.$toast.error( msg );
        });
    },
    logout ( state ) {
        this.$auth.logout().then((data)=> {
            this.$router.replace('/login');
            //this.$toast.success('Success');
        }).catch((err)=> {
            console.log(err)
        });
    }
};

export const actions = {
    update_feature_photo( { commit, state }, para) {
        this.$axios.$post('/api/user' , para ).then( response => {
            if(response.data[0].background == '' ) response.data[0].background = '/images/default_cover.jpg';
            commit('SET_FEATURE_PHOTO' , { 'data':response.data[0]});
        })
        .catch(function (error) {
            console.log(error);
        });
    },
    update_recommended( { commit, state }) {
        this.$axios.$post('/api/user/recommended').then( response => {
            if(response.message != "Unable to find recommended users. Please add more games"){
                commit('SET_RECOMMANDED' , response.data );
            }
        })
        .catch(function (error) {
            console.log(error);
        });
    },
    update_friendlist( { commit, state }) {
        commit('CLEAN_FRIENDLIST');
        this.$axios.$post('/api/friends').then( response => {
            var lastpage = response.meta.last_page;
            for(var getpage = 1 ; getpage <= lastpage ; getpage++){
                this.$axios.$post('/api/friends?page=' + getpage).then( response => {
                    commit('ADD_FRIENDLIST' , response.data );
                });
            }
        })
        .catch(function (error) {
            console.log(error);
        });
    },
    update_requests( { commit, state }) {
        this.$axios.$post('/api/requests').then( response => {
            commit('SET_REQUESTS' , response );
        })
        .catch(function (error) {
            console.log(error);
        });
},
}