export const state = () => ({
    result: [],
    page: 1,
})

export const mutations = {
    SET_RESULT(state , info) {
        state.result = info;
    },
    ADD_RESULT(state , info) {
        state.result.push(...info);
    },
    UPDATE_RESULT(state , info) {
        state.result.splice(info['index'], 1, info['item']);
        //console.log(state.result[info['index']]);
    },
    ADD_TOPRESULT(state , info) {
        state.result.unshift(...info);
    },
    DEL_RESULT(state , info) {
        state.result.splice(info , 1);
    },
    SET_PAGE(state , info) {
        state.page = info;
    },
};

export const actions = {
}