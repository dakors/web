export default ({ app }, inject) => {
    inject('errormsg', (data) => {
        var title = data.message == true ? '' : data.message;
        var content = '';
        var errors = !data.errors == true ? [] : Object.keys(data.errors);
        var error =  !data.error == true ? '' : data.error;

        errors.map(function(objectKey, index) {
            var value = data.errors[objectKey];
            content += value+'<br>';
        });


        return title + '<br>' + content + error;
    }),
    inject('jsonparser', (data) => {
        if(typeof data === 'string'){
            return JSON.parse(data);
        }
        return data;
    }),
    inject('nl2br', (str, replaceMode, isXhtml) => {
        var breakTag = (isXhtml) ? '<br />' : '<br>';
        var replaceStr = (replaceMode) ? '$1'+ breakTag : '$1'+ breakTag +'$2';
        return (str + '').replace(/([^>\r\n]?)(\r\n|\n\r|\r|\n)/g, replaceStr);
    }),
    inject('linkify', (inputText) => {
        if(typeof inputText === 'string'){
            var replacedText, replacePattern1, replacePattern2, replacePattern3;

            //URLs starting with http://, https://, or ftp://
            replacePattern1 = /(\b(https?|ftp):\/\/[-A-Z0-9+&@#\/%?=~_|!:,.;]*[-A-Z0-9+&@#\/%=~_|])/gim;
            replacedText = inputText.replace(replacePattern1, '<a href="$1" target="_blank">$1</a>');

            //URLs starting with "www." (without // before it, or it'd re-link the ones done above).
            replacePattern2 = /(^|[^\/])(www\.[\S]+(\b|$))/gim;
            replacedText = replacedText.replace(replacePattern2, '$1<a href="http://$2" target="_blank">$2</a>');

            //Change email addresses to mailto:: links.
            replacePattern3 = /(([a-zA-Z0-9\-\_\.])+@[a-zA-Z\_]+?(\.[a-zA-Z]{2,6})+)/gim;
            replacedText = replacedText.replace(replacePattern3, '<a href="mailto:$1">$1</a>');

            return replacedText;
        }

    })
}